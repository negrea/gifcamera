package com.afollestad.materialcamera.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import com.afollestad.materialcamera.encoder.AnimatedGifEncoder;
import com.afollestad.materialcamera.model.Size;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by Vlad on 21/04/16.
 */
public class GifGenerator extends AsyncTask<Queue<byte[]>, Void, byte[]> {

    private static final String TAG = GifGenerator.class.getSimpleName();
    AtomicBoolean isRunning;

    AnimatedGifEncoder animatedGifEncoder;
    ByteArrayOutputStream gifBos;

    Size dimensions;
    final Size mGifFrameDimensions = new Size(480, 270);

    int FPS = 6;

    public GifGenerator(AtomicBoolean isRunning, Size dimensions) {
        this.isRunning = isRunning;
        this.dimensions = dimensions;
        initEncoder();
    }

    public GifGenerator(AtomicBoolean isRunning, Size dimensions, int FPS) {
        this.isRunning = isRunning;
        this.dimensions = dimensions;
        this.FPS = FPS;
        initEncoder();
    }

    private void initEncoder() {
        animatedGifEncoder = new AnimatedGifEncoder();
        gifBos = new ByteArrayOutputStream();
        animatedGifEncoder.setDelay(100);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        animatedGifEncoder.start(gifBos);

        Log.d(TAG, "onPreExecute: dimensions " + dimensions.toString());
    }

    @SafeVarargs
    @Override
    protected final byte[] doInBackground(Queue<byte[]>... params) {
        Log.d(TAG, "doInBackground: Is running: " + isRunning);
        int framesCount = 0;
        long globalTimestamp = Calendar.getInstance().getTimeInMillis();
        while (isRunning.get() || !params[0].isEmpty()) {
            long startTimestamp;

            byte[] data = params[0].poll();
            if (data != null) {
                //generating bitmap
                startTimestamp = Calendar.getInstance().getTimeInMillis();
                YuvImage yuvimage = new YuvImage(data, ImageFormat.NV21, dimensions.getWidth(), dimensions.getHeight(), null);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                yuvimage.compressToJpeg(new Rect(0, 0, dimensions.getWidth(), dimensions.getHeight()), 80, baos);

                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inMutable = true;

                Bitmap frameBitmap = BitmapFactory.decodeByteArray(baos.toByteArray(), 0, baos.toByteArray().length, options);
                // Resize to smaller resolution
                frameBitmap = getResizedAndRotatedBitmap(frameBitmap, mGifFrameDimensions.getWidth(), mGifFrameDimensions.getHeight(), -90);
                Log.d(TAG, "doInBackground: " + String.format("Adding frame width %s and height %s.", frameBitmap.getWidth(), frameBitmap.getHeight()));
                Log.d(TAG, "genGIF: End getting resized gif: " + String.format("%.2f", getTimeSinceStarted(startTimestamp)));

                // adding frame to gif
                startTimestamp = Calendar.getInstance().getTimeInMillis();
                animatedGifEncoder.addFrame(frameBitmap);
                Log.d(TAG, "genGIF: End adding frame to gif: " + String.format("%.2f", getTimeSinceStarted(startTimestamp)));

                framesCount++;
            } else {
                Log.d(TAG, "doInBackground: Got null byte array");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        Log.d(TAG, String.format("doInBackground: Finished gif encoding. Added %d frames in %s seconds.", framesCount, String.format("%.2f", getTimeSinceStarted(globalTimestamp))));
        animatedGifEncoder.finish();

        return gifBos.toByteArray();
    }

    @Override
    protected void onPostExecute(byte[] bytes) {
        super.onPostExecute(bytes);

        // Saving the gif
        String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
        File outFile = new File(extStorageDirectory, "test.GIF");
        try {
            BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(outFile));
            bos.write(bytes);
            gifBos.flush();
            gifBos.close();
            bos.flush();
            bos.close();

            Log.d(TAG, outFile.getAbsolutePath() + " Saved");
        } catch (IOException e) {
            e.printStackTrace();
            Log.e(TAG, e.getMessage());
        }
    }

    public Bitmap getResizedAndRotatedBitmap(Bitmap bm, int newWidth, int newHeight, int rotation) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);
        matrix.postRotate(rotation);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);
        bm.recycle();
        return resizedBitmap;
    }

    float getTimeSinceStarted(long started) {
        long second = 1000;
        long timeNow = Calendar.getInstance().getTimeInMillis();
        return (float)(timeNow - started) / second;
    }
}
